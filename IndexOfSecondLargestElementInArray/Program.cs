﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        int largest = int.MinValue;
        int secondLargest = int.MinValue;
        if(x.Length<=1)
        {
            return -1;
        }
        else {
            for (int i = 0; i < x.Length; i++)
            {
                int number = x[i];
                if (number > largest)
                {
                    secondLargest = largest;
                    largest = number;
                }
                else if (number == largest && i < Array.IndexOf(x, largest))
                {
                    largest = number;
                }
                else if (number > secondLargest && number != largest)
                {
                    secondLargest = number;
                }
            }

            return Array.IndexOf(x, secondLargest);
        }
    } 
}