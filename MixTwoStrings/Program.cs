﻿var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        int maxLength = Math.Max(value1.Length, value2.Length);
        string result = "";

        for (int i = 0; i < maxLength; i++)
        {
            if (i < value1.Length)
            {
                result += value1[i];
            }
            if (i < value2.Length)
            {
                result += value2[i];
            }
        }
        return result;
    }
}

